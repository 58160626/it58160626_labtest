CREATE OR REPLACE VIEW StudentAdvise AS
SELECT A.StudentID, A.StudentName, A.StudentSurname , B.Deptname , C.AdvisorName , C.AdvisorSurname
FROM Student AS A 
LEFT JOIN (Department AS B, Advisor AS C) 
ON (B.DeptID = A.DeptID AND C.AdvisorID = A.AdvisorID);
